<?php
/** Enable W3 Total Cache */
define('WP_CACHE', true); // Added by W3 Total Cache

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'shersial_wp61' );

/** MySQL database username */
define( 'DB_USER', 'shersial_wp61' );

/** MySQL database password */
define( 'DB_PASSWORD', 'S[5)of23cp' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'vxduo10gwqsd6tx1qbrhitggxymxtvoftd6cyq21siga0adzwjjw2z1pqpsvg0ov' );
define( 'SECURE_AUTH_KEY',  '1d48rhh2cdgidah0r6zjbrv9zibzdqs8y74erk8kvttm1oybpmn4g5fklqstj4rw' );
define( 'LOGGED_IN_KEY',    '4clbzsjgk76hl6q6tpmh0qbxvfrteycajpjavmkdo8p4om0h9ca60btjjshqach6' );
define( 'NONCE_KEY',        's9qxqwacbrnrpyutumxecnpf08mrz01hodkgsgh6vdur8c2uoqm5qq0gz0zxga1b' );
define( 'AUTH_SALT',        'f22bhv0fpxxqsoibe3kk5zdyznwsblftfg2gma5ynsdxxcn4wco8rf4t2czt0rci' );
define( 'SECURE_AUTH_SALT', '9ydiwsvw5t22pvdjivac2j37vjfxbuibuq1pedca1slmgjumyyl0jelq2oguozly' );
define( 'LOGGED_IN_SALT',   'wb9gftu6year1s5gphmnqbg7tbdrfay8ucxqcikqchdinvgavwmpn3jjnh64mcya' );
define( 'NONCE_SALT',       '2lttpuymic6eakwsbensfys6zakypbzmyyxbh7wmh8jsqnqocapncm1rsjtpnux5' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wpei_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
