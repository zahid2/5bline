<?php
add_action( 'wp_enqueue_scripts', 'aagan_child_enqueue_styles', 100);
function aagan_child_enqueue_styles() {
	wp_enqueue_style( 'aagan-parent', get_theme_file_uri('/style.css') );
}