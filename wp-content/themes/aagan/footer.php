    <?php
        /**
         * aagan_hook_content_after hook.
         * 
         */
        do_action( 'aagan_hook_content_after' );
    ?>

        <!-- **Footer** -->
        <footer id="footer">
            <div class="container">
            <?php
                /**
                 * aagan_footer hook.
                 * 
                 * @hooked aagan_vc_footer_template - 10
                 *
                 */
                do_action( 'aagan_footer' );
            ?>
            </div>
        </footer><!-- **Footer - End** -->

    </div><!-- **Inner Wrapper - End** -->
        
</div><!-- **Wrapper - End** -->
<?php
    
    do_action( 'aagan_hook_bottom' );

    wp_footer();
?>
</body>
</html>