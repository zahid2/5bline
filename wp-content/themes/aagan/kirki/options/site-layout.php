<?php
$config = aagan_kirki_config();

AAGAN_Kirki::add_section( 'dt_site_layout_section', array(
	'title' => esc_html__( 'Site Layout', 'aagan' ),
	'priority' => 20
) );

	# site-layout
	AAGAN_Kirki::add_field( $config, array(
		'type'     => 'radio-image',
		'settings' => 'site-layout',
		'label'    => esc_html__( 'Site Layout', 'aagan' ),
		'section'  => 'dt_site_layout_section',
		'default'  => aagan_defaults('site-layout'),
		'choices' => array(
			'boxed' =>  AAGAN_THEME_URI.'/kirki/assets/images/site-layout/boxed.png',
			'wide' => AAGAN_THEME_URI.'/kirki/assets/images/site-layout/wide.png',
		)
	));

	# site-boxed-layout
	AAGAN_Kirki::add_field( $config, array(
		'type'     => 'switch',
		'settings' => 'site-boxed-layout',
		'label'    => esc_html__( 'Customize Boxed Layout?', 'aagan' ),
		'section'  => 'dt_site_layout_section',
		'default'  => '1',
		'choices'  => array(
			'on'  => esc_attr__( 'Yes', 'aagan' ),
			'off' => esc_attr__( 'No', 'aagan' )
		),
		'active_callback' => array(
			array( 'setting' => 'site-layout', 'operator' => '==', 'value' => 'boxed' ),
		)			
	));

	# body-bg-type
	AAGAN_Kirki::add_field( $config, array(
		'type' => 'select',
		'settings' => 'body-bg-type',
		'label'    => esc_html__( 'Background Type', 'aagan' ),
		'section'  => 'dt_site_layout_section',
		'multiple' => 1,
		'default'  => 'none',
		'choices'  => array(
			'pattern' => esc_attr__( 'Predefined Patterns', 'aagan' ),
			'upload' => esc_attr__( 'Set Pattern', 'aagan' ),
			'none' => esc_attr__( 'None', 'aagan' ),
		),
		'active_callback' => array(
			array( 'setting' => 'site-layout', 'operator' => '==', 'value' => 'boxed' ),
			array( 'setting' => 'site-boxed-layout', 'operator' => '==', 'value' => '1' )
		)
	));

	# body-bg-pattern
	AAGAN_Kirki::add_field( $config, array(
		'type'     => 'radio-image',
		'settings' => 'body-bg-pattern',
		'label'    => esc_html__( 'Predefined Patterns', 'aagan' ),
		'description'    => esc_html__( 'Add Background for body', 'aagan' ),
		'section'  => 'dt_site_layout_section',
		'output' => array(
			array( 'element' => 'body' , 'property' => 'background-image' )
		),
		'choices' => array(
			AAGAN_THEME_URI.'/kirki/assets/images/site-layout/pattern1.jpg'=> AAGAN_THEME_URI.'/kirki/assets/images/site-layout/pattern1.jpg',
			AAGAN_THEME_URI.'/kirki/assets/images/site-layout/pattern2.jpg'=> AAGAN_THEME_URI.'/kirki/assets/images/site-layout/pattern2.jpg',
			AAGAN_THEME_URI.'/kirki/assets/images/site-layout/pattern3.jpg'=> AAGAN_THEME_URI.'/kirki/assets/images/site-layout/pattern3.jpg',
			AAGAN_THEME_URI.'/kirki/assets/images/site-layout/pattern4.jpg'=> AAGAN_THEME_URI.'/kirki/assets/images/site-layout/pattern4.jpg',
			AAGAN_THEME_URI.'/kirki/assets/images/site-layout/pattern5.jpg'=> AAGAN_THEME_URI.'/kirki/assets/images/site-layout/pattern5.jpg',
			AAGAN_THEME_URI.'/kirki/assets/images/site-layout/pattern6.jpg'=> AAGAN_THEME_URI.'/kirki/assets/images/site-layout/pattern6.jpg',
			AAGAN_THEME_URI.'/kirki/assets/images/site-layout/pattern7.jpg'=> AAGAN_THEME_URI.'/kirki/assets/images/site-layout/pattern7.jpg',
			AAGAN_THEME_URI.'/kirki/assets/images/site-layout/pattern8.jpg'=> AAGAN_THEME_URI.'/kirki/assets/images/site-layout/pattern8.jpg',
			AAGAN_THEME_URI.'/kirki/assets/images/site-layout/pattern9.jpg'=> AAGAN_THEME_URI.'/kirki/assets/images/site-layout/pattern9.jpg',
			AAGAN_THEME_URI.'/kirki/assets/images/site-layout/pattern10.jpg'=> AAGAN_THEME_URI.'/kirki/assets/images/site-layout/pattern10.jpg',
			AAGAN_THEME_URI.'/kirki/assets/images/site-layout/pattern11.jpg'=> AAGAN_THEME_URI.'/kirki/assets/images/site-layout/pattern11.jpg',
			AAGAN_THEME_URI.'/kirki/assets/images/site-layout/pattern12.jpg'=> AAGAN_THEME_URI.'/kirki/assets/images/site-layout/pattern12.jpg',
			AAGAN_THEME_URI.'/kirki/assets/images/site-layout/pattern13.jpg'=> AAGAN_THEME_URI.'/kirki/assets/images/site-layout/pattern13.jpg',
			AAGAN_THEME_URI.'/kirki/assets/images/site-layout/pattern14.jpg'=> AAGAN_THEME_URI.'/kirki/assets/images/site-layout/pattern14.jpg',
			AAGAN_THEME_URI.'/kirki/assets/images/site-layout/pattern15.jpg'=> AAGAN_THEME_URI.'/kirki/assets/images/site-layout/pattern15.jpg',
		),
		'active_callback' => array(
			array( 'setting' => 'body-bg-type', 'operator' => '==', 'value' => 'pattern' ),
			array( 'setting' => 'site-layout', 'operator' => '==', 'value' => 'boxed' ),
			array( 'setting' => 'site-boxed-layout', 'operator' => '==', 'value' => '1' )
		)						
	));

	# body-bg-image
	AAGAN_Kirki::add_field( $config, array(
		'type' => 'image',
		'settings' => 'body-bg-image',
		'label'    => esc_html__( 'Background Image', 'aagan' ),
		'description'    => esc_html__( 'Add Background Image for body', 'aagan' ),
		'section'  => 'dt_site_layout_section',
		'output' => array(
			array( 'element' => 'body' , 'property' => 'background-image' )
		),
		'active_callback' => array(
			array( 'setting' => 'body-bg-type', 'operator' => '==', 'value' => 'upload' ),
			array( 'setting' => 'site-layout', 'operator' => '==', 'value' => 'boxed' ),
			array( 'setting' => 'site-boxed-layout', 'operator' => '==', 'value' => '1' )
		)
	));

	# body-bg-position
	AAGAN_Kirki::add_field( $config, array(
		'type' => 'select',
		'settings' => 'body-bg-position',
		'label'    => esc_html__( 'Background Position', 'aagan' ),
		'section'  => 'dt_site_layout_section',
		'output' => array(
			array( 'element' => 'body' , 'property' => 'background-position' )
		),
		'default' => 'center',
		'multiple' => 1,
		'choices' => aagan_image_positions(),
		'active_callback' => array(
			array( 'setting' => 'body-bg-type', 'operator' => 'contains', 'value' => array( 'pattern', 'upload') ),
			array( 'setting' => 'site-layout', 'operator' => '==', 'value' => 'boxed' ),
			array( 'setting' => 'site-boxed-layout', 'operator' => '==', 'value' => '1' )
		)
	));

	# body-bg-repeat
	AAGAN_Kirki::add_field( $config, array(
		'type' => 'select',
		'settings' => 'body-bg-repeat',
		'label'    => esc_html__( 'Background Repeat', 'aagan' ),
		'section'  => 'dt_site_layout_section',
		'output' => array(
			array( 'element' => 'body' , 'property' => 'background-repeat' )
		),
		'default' => 'repeat',
		'multiple' => 1,
		'choices' => aagan_image_repeats(),
		'active_callback' => array(
			array( 'setting' => 'body-bg-type', 'operator' => 'contains', 'value' => array( 'pattern', 'upload' ) ),
			array( 'setting' => 'site-layout', 'operator' => '==', 'value' => 'boxed' ),
			array( 'setting' => 'site-boxed-layout', 'operator' => '==', 'value' => '1' )
		)
	));	