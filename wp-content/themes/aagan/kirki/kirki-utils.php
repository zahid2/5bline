<?php
function aagan_kirki_config() {
	return 'aagan_kirki_config';
}

function aagan_defaults( $key = '' ) {
	$defaults = array();

	# site identify
	$defaults['use-custom-logo'] = '1';
	$defaults['custom-logo'] = AAGAN_THEME_URI.'/images/light-logo.png';
	$defaults['custom-light-logo'] = AAGAN_THEME_URI.'/images/logo.png';
	$defaults['site_icon'] = AAGAN_THEME_URI.'/images/favicon.ico';

	# site layout
	$defaults['site-layout'] = 'wide';

	# site skin
	$defaults['primary-color'] = '#29d9c2';
	$defaults['secondary-color'] = '#23c5b0';
	$defaults['tertiary-color'] = '#3fddc8';

	$defaults['body-bg-color']      = '#ffffff';
	$defaults['body-content-color'] = '#909090';
	$defaults['body-a-color']       = '#29d9c2';
	$defaults['body-a-hover-color'] = '#23c5b0';

	$defaults['custom-title-color'] = '#ffffff';

	# site breadcrumb
	$defaults['customize-breadcrumb-title-typo'] = '1';
	$defaults['breadcrumb-title-typo'] = array( 'font-family' => 'Montserrat',
		'variant' => '700',
		'subsets' => array( 'latin-ext' ),
		'font-size' => '44px',
		'line-height' => '',
		'letter-spacing' => '',
		'color' => '#fff',
		'text-align' => 'unset',
		'text-transform' => 'capitalize' );
	$defaults['customize-breadcrumb-typo'] = '0';
	$defaults['breadcrumb-typo'] = array( 'font-family' => 'Montserrat',
		'variant' => 'regular',
		'subsets' => array( 'latin-ext' ),
		'font-size' => '13px',
		'line-height' => '',
		'letter-spacing' => '0',
		'color' => '#fff',
		'text-align' => 'unset',
		'text-transform' => 'none' );

	# site footer
	$defaults['customize-footer-title-typo'] = '1';
	$defaults['footer-title-typo'] = array( 'font-family' => 'Montserrat',
		'variant' => '700',
		'subsets' => array( 'latin-ext' ),
		'font-size' => '30px',
		'line-height' => '',
		'letter-spacing' => '0',
		'color' => '#000000',
		'text-align' => 'unset',
		'text-transform' => 'none' );
	$defaults['customize-footer-content-typo'] = '1';
	$defaults['footer-content-typo'] = array( 'font-family' => 'Lato',
		'variant' => 'regular',
		'subsets' => array( 'latin-ext' ),
		'font-size' => '16px',
		'line-height' => '26px',
		'letter-spacing' => '0',
		'color' => '#909090',
		'text-align' => 'unset',
		'text-transform' => 'none' );

	# site typography
	$defaults['customize-body-h1-typo'] = '1';
	$defaults['h1'] = array(
		'font-family' => 'Montserrat',
		'variant' => '700',
		'font-size' => '40px',
		'line-height' => '',
		'letter-spacing' => '0',
		'color' => '#121212',
		'text-align' => 'unset',
		'text-transform' => 'none'
	);
	$defaults['customize-body-h2-typo'] = '1';
	$defaults['h2'] = array(
		'font-family' => 'Montserrat',
		'variant' => '700',
		'font-size' => '36px',
		'line-height' => '',
		'letter-spacing' => '0',
		'color' => '#121212',
		'text-align' => 'unset',
		'text-transform' => 'none'
	);
	$defaults['customize-body-h3-typo'] = '1';
	$defaults['h3'] = array(
		'font-family' => 'Montserrat',
		'variant' => '700',
		'font-size' => '32px',
		'line-height' => '',
		'letter-spacing' => '0',
		'color' => '#121212',
		'text-align' => 'unset',
		'text-transform' => 'none'
	);
	$defaults['customize-body-h4-typo'] = '1';
	$defaults['h4'] = array(
		'font-family' => 'Montserrat',
		'variant' => '700',
		'font-size' => '28px',
		'line-height' => '',
		'letter-spacing' => '0',
		'color' => '#121212',
		'text-align' => 'unset',
		'text-transform' => 'none'
	);
	$defaults['customize-body-h5-typo'] = '1';
	$defaults['h5'] = array(
		'font-family' => 'Montserrat',
		'variant' => '700',
		'font-size' => '24px',
		'line-height' => '',
		'letter-spacing' => '0',
		'color' => '#121212',
		'text-align' => 'unset',
		'text-transform' => 'none'
	);
	$defaults['customize-body-h6-typo'] = '1';
	$defaults['h6'] = array(
		'font-family' => 'Montserrat',
		'variant' => '700',
		'font-size' => '20px',
		'line-height' => '',
		'letter-spacing' => '0',
		'color' => '#121212',
		'text-align' => 'unset',
		'text-transform' => 'none'
	);
	$defaults['customize-body-content-typo'] = '1';
	$defaults['body-content-typo'] = array(
		'font-family' => 'Lato',
		'variant' => 'regular',
		'font-size' => '16px',
		'line-height' => '26px',
		'letter-spacing' => '',
		'color' => '#909090',
		'text-align' => 'unset',
		'text-transform' => 'none'
	);

	$defaults['footer-content-a-color'] = '';
	$defaults['footer-content-a-hover-color'] = '';	

	if( !empty( $key ) && array_key_exists( $key, $defaults) ) {
		return $defaults[$key];
	}

	return '';
}

function aagan_image_positions() {

	$positions = array( "top left" => esc_attr__('Top Left','aagan'),
		"top center"    => esc_attr__('Top Center','aagan'),
		"top right"     => esc_attr__('Top Right','aagan'),
		"center left"   => esc_attr__('Center Left','aagan'),
		"center center" => esc_attr__('Center Center','aagan'),
		"center right"  => esc_attr__('Center Right','aagan'),
		"bottom left"   => esc_attr__('Bottom Left','aagan'),
		"bottom center" => esc_attr__('Bottom Center','aagan'),
		"bottom right"  => esc_attr__('Bottom Right','aagan'),
	);

	return $positions;
}

function aagan_image_repeats() {

	$image_repeats = array( "repeat" => esc_attr__('Repeat','aagan'),
		"repeat-x"  => esc_attr__('Repeat in X-axis','aagan'),
		"repeat-y"  => esc_attr__('Repeat in Y-axis','aagan'),
		"no-repeat" => esc_attr__('No Repeat','aagan')
	);

	return $image_repeats;
}

function aagan_border_styles() {

	$image_repeats = array(
		"none"	 => esc_attr__('None','aagan'),
		"dotted" => esc_attr__('Dotted','aagan'),
		"dashed" => esc_attr__('Dashed','aagan'),
		"solid"	 => esc_attr__('Solid','aagan'),
		"double" => esc_attr__('Double','aagan'),
		"groove" => esc_attr__('Groove','aagan'),
		"ridge"	 => esc_attr__('Ridge','aagan'),
	);

	return $image_repeats;
}

function aagan_animations() {

	$animations = array(
		'' 					 => esc_html__('Default','aagan'),	
		"bigEntrance"        =>  esc_attr__("bigEntrance",'aagan'),
        "bounce"             =>  esc_attr__("bounce",'aagan'),
        "bounceIn"           =>  esc_attr__("bounceIn",'aagan'),
        "bounceInDown"       =>  esc_attr__("bounceInDown",'aagan'),
        "bounceInLeft"       =>  esc_attr__("bounceInLeft",'aagan'),
        "bounceInRight"      =>  esc_attr__("bounceInRight",'aagan'),
        "bounceInUp"         =>  esc_attr__("bounceInUp",'aagan'),
        "bounceOut"          =>  esc_attr__("bounceOut",'aagan'),
        "bounceOutDown"      =>  esc_attr__("bounceOutDown",'aagan'),
        "bounceOutLeft"      =>  esc_attr__("bounceOutLeft",'aagan'),
        "bounceOutRight"     =>  esc_attr__("bounceOutRight",'aagan'),
        "bounceOutUp"        =>  esc_attr__("bounceOutUp",'aagan'),
        "expandOpen"         =>  esc_attr__("expandOpen",'aagan'),
        "expandUp"           =>  esc_attr__("expandUp",'aagan'),
        "fadeIn"             =>  esc_attr__("fadeIn",'aagan'),
        "fadeInDown"         =>  esc_attr__("fadeInDown",'aagan'),
        "fadeInDownBig"      =>  esc_attr__("fadeInDownBig",'aagan'),
        "fadeInLeft"         =>  esc_attr__("fadeInLeft",'aagan'),
        "fadeInLeftBig"      =>  esc_attr__("fadeInLeftBig",'aagan'),
        "fadeInRight"        =>  esc_attr__("fadeInRight",'aagan'),
        "fadeInRightBig"     =>  esc_attr__("fadeInRightBig",'aagan'),
        "fadeInUp"           =>  esc_attr__("fadeInUp",'aagan'),
        "fadeInUpBig"        =>  esc_attr__("fadeInUpBig",'aagan'),
        "fadeOut"            =>  esc_attr__("fadeOut",'aagan'),
        "fadeOutDownBig"     =>  esc_attr__("fadeOutDownBig",'aagan'),
        "fadeOutLeft"        =>  esc_attr__("fadeOutLeft",'aagan'),
        "fadeOutLeftBig"     =>  esc_attr__("fadeOutLeftBig",'aagan'),
        "fadeOutRight"       =>  esc_attr__("fadeOutRight",'aagan'),
        "fadeOutUp"          =>  esc_attr__("fadeOutUp",'aagan'),
        "fadeOutUpBig"       =>  esc_attr__("fadeOutUpBig",'aagan'),
        "flash"              =>  esc_attr__("flash",'aagan'),
        "flip"               =>  esc_attr__("flip",'aagan'),
        "flipInX"            =>  esc_attr__("flipInX",'aagan'),
        "flipInY"            =>  esc_attr__("flipInY",'aagan'),
        "flipOutX"           =>  esc_attr__("flipOutX",'aagan'),
        "flipOutY"           =>  esc_attr__("flipOutY",'aagan'),
        "floating"           =>  esc_attr__("floating",'aagan'),
        "hatch"              =>  esc_attr__("hatch",'aagan'),
        "hinge"              =>  esc_attr__("hinge",'aagan'),
        "lightSpeedIn"       =>  esc_attr__("lightSpeedIn",'aagan'),
        "lightSpeedOut"      =>  esc_attr__("lightSpeedOut",'aagan'),
        "pullDown"           =>  esc_attr__("pullDown",'aagan'),
        "pullUp"             =>  esc_attr__("pullUp",'aagan'),
        "pulse"              =>  esc_attr__("pulse",'aagan'),
        "rollIn"             =>  esc_attr__("rollIn",'aagan'),
        "rollOut"            =>  esc_attr__("rollOut",'aagan'),
        "rotateIn"           =>  esc_attr__("rotateIn",'aagan'),
        "rotateInDownLeft"   =>  esc_attr__("rotateInDownLeft",'aagan'),
        "rotateInDownRight"  =>  esc_attr__("rotateInDownRight",'aagan'),
        "rotateInUpLeft"     =>  esc_attr__("rotateInUpLeft",'aagan'),
        "rotateInUpRight"    =>  esc_attr__("rotateInUpRight",'aagan'),
        "rotateOut"          =>  esc_attr__("rotateOut",'aagan'),
        "rotateOutDownRight" =>  esc_attr__("rotateOutDownRight",'aagan'),
        "rotateOutUpLeft"    =>  esc_attr__("rotateOutUpLeft",'aagan'),
        "rotateOutUpRight"   =>  esc_attr__("rotateOutUpRight",'aagan'),
        "shake"              =>  esc_attr__("shake",'aagan'),
        "slideDown"          =>  esc_attr__("slideDown",'aagan'),
        "slideExpandUp"      =>  esc_attr__("slideExpandUp",'aagan'),
        "slideLeft"          =>  esc_attr__("slideLeft",'aagan'),
        "slideRight"         =>  esc_attr__("slideRight",'aagan'),
        "slideUp"            =>  esc_attr__("slideUp",'aagan'),
        "stretchLeft"        =>  esc_attr__("stretchLeft",'aagan'),
        "stretchRight"       =>  esc_attr__("stretchRight",'aagan'),
        "swing"              =>  esc_attr__("swing",'aagan'),
        "tada"               =>  esc_attr__("tada",'aagan'),
        "tossing"            =>  esc_attr__("tossing",'aagan'),
        "wobble"             =>  esc_attr__("wobble",'aagan'),
        "fadeOutDown"        =>  esc_attr__("fadeOutDown",'aagan'),
        "fadeOutRightBig"    =>  esc_attr__("fadeOutRightBig",'aagan'),
        "rotateOutDownLeft"  =>  esc_attr__("rotateOutDownLeft",'aagan')
    );

	return $animations;
}

function aagan_custom_fonts( $standard_fonts ){

	$custom_fonts = array();

	$fonts = cs_get_option('custom_font_fields');
	if( count( $fonts ) > 0 ):
		foreach( $fonts as $font ):
			$custom_fonts[$font['custom_font_name']] = array(
				'label' => $font['custom_font_name'],
				'variants' => array( '100', '100italic', '200', '200italic', '300', '300italic', 'regular', 'italic', '500', '500italic', '600', '600italic', '700', '700italic', '800', '800italic', '900', '900italic' ),
				'stack' => $font['custom_font_name'] . ', sans-serif'
			);
		endforeach;
	endif;

	return array_merge_recursive( $custom_fonts, $standard_fonts );
}
add_filter( 'kirki/fonts/standard_fonts', 'aagan_custom_fonts', 20 );