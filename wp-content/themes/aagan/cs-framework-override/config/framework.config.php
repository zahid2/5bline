<?php if ( ! defined( 'ABSPATH' ) ) { die; } // Cannot access pages directly.
// ===============================================================================================
// -----------------------------------------------------------------------------------------------
// FRAMEWORK SETTINGS
// -----------------------------------------------------------------------------------------------
// ===============================================================================================
$settings           = array(
  'menu_title'      => constant('AAGAN_THEME_NAME').' '.esc_html__('Options', 'aagan'),
  'menu_type'       => 'theme', // menu, submenu, options, theme, etc.
  'menu_slug'       => 'cs-framework',
  'ajax_save'       => true,
  'show_reset_all'  => false,
  'framework_title' => sprintf(esc_html__('Designthemes Framework %1$s', 'aagan'), '<small>'.esc_html__('by Designthemes', 'aagan').'</small>'),
);

// ===============================================================================================
// -----------------------------------------------------------------------------------------------
// FRAMEWORK OPTIONS
// -----------------------------------------------------------------------------------------------
// ===============================================================================================
$options        = array();

$options[]      = array(
  'name'        => 'general',
  'title'       => esc_html__('General', 'aagan'),
  'icon'        => 'fa fa-gears',

  'fields'      => array(

	array(
	  'type'    => 'subheading',
	  'content' => esc_html__( 'General Options', 'aagan' ),
	),
	
	array(
		'id'	=> 'header',
		'type'	=> 'select',
		'title'	=> esc_html__('Site Header', 'aagan'),
		'class'	=> 'chosen',
		'options'	=> 'posts',
		'query_args'	=> array(
			'post_type'	=> 'dt_headers',
			'orderby'	=> 'title',
			'order'	=> 'ASC',
			'posts_per_page' => -1
		),
		'default_option'	=> esc_attr__('Select Header', 'aagan'),
		'attributes'	=> array ( 'style'	=> 'width:50%'),
		'info'	=> esc_html__('Select default header.','aagan'),
	),
	
	array(
		'id'	=> 'footer',
		'type'	=> 'select',
		'title'	=> esc_html__('Site Footer', 'aagan'),
		'class'	=> 'chosen',
		'options'	=> 'posts',
		'query_args'	=> array(
			'post_type'	=> 'dt_footers',
			'orderby'	=> 'title',
			'order'	=> 'ASC',
			'posts_per_page' => -1
		),
		'default_option'	=> esc_attr__('Select Footer', 'aagan'),
		'attributes'	=> array ( 'style'	=> 'width:50%'),
		'info'	=> esc_html__('Select defaultfooter.','aagan'),
	),

	array(
	  'id'  	 => 'use-site-loader',
	  'type'  	 => 'switcher',
	  'title' 	 => esc_html__('Site Loader', 'aagan'),
	  'info'	 => esc_html__('YES! to use site loader.', 'aagan')
	),	

	array(
	  'id'  	 => 'enable-stylepicker',
	  'type'  	 => 'switcher',
	  'title' 	 => esc_html__('Style Picker', 'aagan'),
	  'info'	 => esc_html__('YES! to show the style picker.', 'aagan')
	),		

	array(
	  'id'  	 => 'show-pagecomments',
	  'type'  	 => 'switcher',
	  'title' 	 => esc_html__('Globally Show Page Comments', 'aagan'),
	  'info'	 => esc_html__('YES! to show comments on all the pages. This will globally override your "Allow comments" option under your page "Discussion" settings.', 'aagan'),
	  'default'  => true,
	),

	array(
	  'id'  	 => 'showall-pagination',
	  'type'  	 => 'switcher',
	  'title' 	 => esc_html__('Show all pages in Pagination', 'aagan'),
	  'info'	 => esc_html__('YES! to show all the pages instead of dots near the current page.', 'aagan')
	),



	array(
	  'id'      => 'google-map-key',
	  'type'    => 'text',
	  'title'   => esc_html__('Google Map API Key', 'aagan'),
	  'after' 	=> '<p class="cs-text-info">'.esc_html__('Put a valid google account api key here', 'aagan').'</p>',
	),

	array(
	  'id'      => 'mailchimp-key',
	  'type'    => 'text',
	  'title'   => esc_html__('Mailchimp API Key', 'aagan'),
	  'after' 	=> '<p class="cs-text-info">'.esc_html__('Put a valid mailchimp account api key here', 'aagan').'</p>',
	),

  ),
);

$options[]      = array(
  'name'        => 'layout_options',
  'title'       => esc_html__('Layout Options', 'aagan'),
  'icon'        => 'dashicons dashicons-exerpt-view',
  'sections' => array(

	// -----------------------------------------
	// Header Options
	// -----------------------------------------
	array(
	  'name'      => 'breadcrumb_options',
	  'title'     => esc_html__('Breadcrumb Options', 'aagan'),
	  'icon'      => 'fa fa-sitemap',

		'fields'      => array(

		  array(
			'type'    => 'subheading',
			'content' => esc_html__( "Breadcrumb Options", 'aagan' ),
		  ),

		  array(
			'id'  		 => 'show-breadcrumb',
			'type'  	 => 'switcher',
			'title' 	 => esc_html__('Show Breadcrumb', 'aagan'),
			'info'		 => esc_html__('YES! to display breadcrumb for all pages.', 'aagan'),
			'default' 	 => true,
		  ),

		  array(
			'id'           => 'breadcrumb-delimiter',
			'type'         => 'icon',
			'title'        => esc_html__('Breadcrumb Delimiter', 'aagan'),
			'info'         => esc_html__('Choose delimiter style to display on breadcrumb section.', 'aagan'),
		  ),

		  array(
			'id'           => 'breadcrumb-style',
			'type'         => 'select',
			'title'        => esc_html__('Breadcrumb Style', 'aagan'),
			'options'      => array(
			  'default' 							=> esc_html__('Default', 'aagan'),
			  'aligncenter'    						=> esc_html__('Align Center', 'aagan'),
			  'alignright'  						=> esc_html__('Align Right', 'aagan'),
			  'breadcrumb-left'    					=> esc_html__('Left Side Breadcrumb', 'aagan'),
			  'breadcrumb-right'     				=> esc_html__('Right Side Breadcrumb', 'aagan'),
			  'breadcrumb-top-right-title-center'  	=> esc_html__('Top Right Title Center', 'aagan'),
			  'breadcrumb-top-left-title-center'  	=> esc_html__('Top Left Title Center', 'aagan'),
			),
			'class'        => 'chosen',
			'default'      => 'breadcrumb-right',
			'info'         => esc_html__('Choose alignment style to display on breadcrumb section.', 'aagan'),
		  ),

		  array(
			  'id'                 => 'breadcrumb-position',
			  'type'               => 'select',
			  'title'              => esc_html__('Position', 'aagan' ),
			  'options'            => array(
				  'header-top-absolute'    => esc_html__('Behind the Header','aagan'),
				  'header-top-relative'    => esc_html__('Default','aagan'),
			  ),
			  'class'        => 'chosen',
			  'default'      => 'header-top-relative',
			  'info'         => esc_html__('Choose position of breadcrumb section.', 'aagan'),
		  ),

		  array(
			'id'    => 'breadcrumb_background',
			'type'  => 'background',
			'title' => esc_html__('Background', 'aagan'),
			'desc'  => esc_html__('Choose background options for breadcrumb title section.', 'aagan'),
			'default' => array (
			  'attachment'  => 'fixed',
		  	),
		  ),

		),
	),

  ),
);

$options[]      = array(
  'name'        => 'allpage_options',
  'title'       => esc_html__('All Page Options', 'aagan'),
  'icon'        => 'fa fa-files-o',
  'sections' => array(

	// -----------------------------------------
	// Post Options
	// -----------------------------------------
	array(
	  'name'      => 'post_options',
	  'title'     => esc_html__('Post Options', 'aagan'),
	  'icon'      => 'fa fa-file',

		'fields'      => array(

		  array(
			'type'    => 'subheading',
			'content' => esc_html__( "Single Post Options", 'aagan' ),
		  ),
		
		  array(
			'id'  		 => 'single-post-authorbox',
			'type'  	 => 'switcher',
			'title' 	 => esc_html__('Single Author Box', 'aagan'),
			'info'		 => esc_html__('YES! to display author box in single blog posts.', 'aagan')
		  ),

		  array(
			'id'  		 => 'single-post-related',
			'type'  	 => 'switcher',
			'title' 	 => esc_html__('Single Related Posts', 'aagan'),
			'info'		 => esc_html__('YES! to display related blog posts in single posts.', 'aagan')
		  ),

		  array(
			'id'  		 => 'single-post-navigation',
			'type'  	 => 'switcher',
			'title' 	 => esc_html__('Single Post Navigation', 'aagan'),
			'info'		 => esc_html__('YES! to display post navigation in single posts.', 'aagan')
		  ),

		  array(
			'id'  		 => 'single-post-comments',
			'type'  	 => 'switcher',
			'title' 	 => esc_html__('Posts Comments', 'aagan'),
			'info'		 => esc_html__('YES! to display single blog post comments.', 'aagan'),
			'default' 	 => true,
		  ),

		  array(
			'type'    => 'subheading',
			'content' => esc_html__( "Post Archives Page Layout", 'aagan' ),
		  ),

		  array(
			'id'      	 => 'post-archives-page-layout',
			'type'       => 'image_select',
			'title'      => esc_html__('Page Layout', 'aagan'),
			'options'    => array(
			  'content-full-width'   => AAGAN_THEME_URI . '/cs-framework-override/images/without-sidebar.png',
			  'with-left-sidebar'    => AAGAN_THEME_URI . '/cs-framework-override/images/left-sidebar.png',
			  'with-right-sidebar'   => AAGAN_THEME_URI . '/cs-framework-override/images/right-sidebar.png',
			  'with-both-sidebar'    => AAGAN_THEME_URI . '/cs-framework-override/images/both-sidebar.png',
			),
			'default'      => 'content-full-width',
			'attributes'   => array(
			  'data-depend-id' => 'post-archives-page-layout',
			),
		  ),

		  array(
			'id'  		 => 'show-standard-left-sidebar-for-post-archives',
			'type'  	 => 'switcher',
			'title' 	 => esc_html__('Show Standard Left Sidebar', 'aagan'),
			'dependency' => array( 'post-archives-page-layout', 'any', 'with-left-sidebar,with-both-sidebar' ),
		  ),

		  array(
			'id'  		 => 'show-standard-right-sidebar-for-post-archives',
			'type'  	 => 'switcher',
			'title' 	 => esc_html__('Show Standard Right Sidebar', 'aagan'),
			'dependency' => array( 'post-archives-page-layout', 'any', 'with-right-sidebar,with-both-sidebar' ),
		  ),

		  array(
			'type'    => 'subheading',
			'content' => esc_html__( "Post Archives Post Layout", 'aagan' ),
		  ),

		  array(
			'id'      	   => 'post-archives-post-layout',
			'type'         => 'image_select',
			'title'        => esc_html__('Post Layout', 'aagan'),
			'options'      => array(
			  'one-column' 		  => AAGAN_THEME_URI . '/cs-framework-override/images/one-column.png',
			  'one-half-column'   => AAGAN_THEME_URI . '/cs-framework-override/images/one-half-column.png',
			  'one-third-column'  => AAGAN_THEME_URI . '/cs-framework-override/images/one-third-column.png',
			  '1-2-2'			  => AAGAN_THEME_URI . '/cs-framework-override/images/1-2-2.png',
			  '1-2-2-1-2-2' 	  => AAGAN_THEME_URI . '/cs-framework-override/images/1-2-2-1-2-2.png',
			  '1-3-3-3'			  => AAGAN_THEME_URI . '/cs-framework-override/images/1-3-3-3.png',
			  '1-3-3-3-1' 		  => AAGAN_THEME_URI . '/cs-framework-override/images/1-3-3-3-1.png',
			),
			'default'      => 'one-half-column',
		  ),

		  array(
			'id'           => 'post-style',
			'type'         => 'select',
			'title'        => esc_html__('Post Style', 'aagan'),
			'options'      => array(
			  'blog-default-style' 		=> esc_html__('Default', 'aagan'),
			  'entry-date-left'      	=> esc_html__('Date Left', 'aagan'),
			  'entry-date-left outer-frame-border'      	=> esc_html__('Date Left Modern', 'aagan'),
			  'entry-date-author-left'  => esc_html__('Date and Author Left', 'aagan'),
			  'blog-modern-style'       => esc_html__('Modern', 'aagan'),
			  'bordered'      			=> esc_html__('Bordered', 'aagan'),
			  'classic'      			=> esc_html__('Classic', 'aagan'),
			  'entry-overlay-style' 	=> esc_html__('Trendy', 'aagan'),
			  'overlap' 				=> esc_html__('Overlap', 'aagan'),
			  'entry-center-align'		=> esc_html__('Stripe', 'aagan'),
			  'entry-fashion-style'	 	=> esc_html__('Fashion', 'aagan'),
			  'entry-minimal-bordered' 	=> esc_html__('Minimal Bordered', 'aagan'),
			  'blog-medium-style'       => esc_html__('Medium', 'aagan'),
			  'blog-medium-style dt-blog-medium-highlight'     					 => esc_html__('Medium Hightlight', 'aagan'),
			  'blog-medium-style dt-blog-medium-highlight dt-sc-skin-highlight'  => esc_html__('Medium Skin Highlight', 'aagan'),
			),
			'class'        => 'chosen',
			'default'      => 'blog-default-style',
			'info'         => esc_html__('Choose post style to display post archives pages.', 'aagan'),
		  ),

		  array(
			'id'  		 => 'post-archives-enable-excerpt',
			'type'  	 => 'switcher',
			'title' 	 => esc_html__('Allow Excerpt', 'aagan'),
			'info'		 => esc_html__('YES! to allow excerpt', 'aagan'),
			'default'    => true,
		  ),

		  array(
			'id'  		 => 'post-archives-excerpt',
			'type'  	 => 'number',
			'title' 	 => esc_html__('Excerpt Length', 'aagan'),
			'after'		 => '<span class="cs-text-desc">&nbsp;'.esc_html__('Put Excerpt Length', 'aagan').'</span>',
			'default' 	 => 40,
		  ),

		  array(
			'id'  		 => 'post-archives-enable-readmore',
			'type'  	 => 'switcher',
			'title' 	 => esc_html__('Read More', 'aagan'),
			'info'		 => esc_html__('YES! to enable read more button', 'aagan'),
			'default'	 => true,
		  ),

		  array(
			'id'  		 => 'post-archives-readmore',
			'type'  	 => 'textarea',
			'title' 	 => esc_html__('Read More Shortcode', 'aagan'),
			'info'		 => esc_html__('Paste any button shortcode here', 'aagan'),
			'default'	 => '[dt_sc_button title="'.esc_attr__('Read More', 'aagan').'" style="filled" icon_type="fontawesome" iconalign="icon-right with-icon" iconclass="fa fa-long-arrow-right" class="type1"]',
		  ),

		  array(
			'type'    => 'subheading',
			'content' => esc_html__( "Single Post & Post Archive options", 'aagan' ),
		  ),

		  array(
			'id'      => 'post-format-meta',
			'type'    => 'switcher',
			'title'   => esc_html__('Post Format Meta', 'aagan' ),
			'info'	  => esc_html__('YES! to show post format meta information', 'aagan'),
			'default' => false
		  ),

		  array(
			'id'      => 'post-author-meta',
			'type'    => 'switcher',
			'title'   => esc_html__('Author Meta', 'aagan' ),
			'info'	  => esc_html__('YES! to show post author meta information', 'aagan'),
			'default' => true
		  ),

		  array(
			'id'      => 'post-date-meta',
			'type'    => 'switcher',
			'title'   => esc_html__('Date Meta', 'aagan' ),
			'info'	  => esc_html__('YES! to show post date meta information', 'aagan'),
			'default' => true
		  ),

		  array(
			'id'      => 'post-comment-meta',
			'type'    => 'switcher',
			'title'   => esc_html__('Comment Meta', 'aagan' ),
			'info'	  => esc_html__('YES! to show post comment meta information', 'aagan'),
			'default' => true
		  ),

		  array(
			'id'      => 'post-category-meta',
			'type'    => 'switcher',
			'title'   => esc_html__('Category Meta', 'aagan' ),
			'info'	  => esc_html__('YES! to show post category information', 'aagan'),
			'default' => true
		  ),

		  array(
			'id'      => 'post-tag-meta',
			'type'    => 'switcher',
			'title'   => esc_html__('Tag Meta', 'aagan' ),
			'info'	  => esc_html__('YES! to show post tag information', 'aagan'),
			'default' => true
			),
			
			array(
				'id'      => 'post-likes',
				'type'    => 'switcher',
				'title'   => esc_html__('Post Likes', 'aagan' ),
				'info'    => esc_html__('YES! to show post likes information', 'aagan'),
				'default' => true
			),

			array(
				'id'      => 'post-views',
				'type'    => 'switcher',
				'title'   => esc_html__('Post Views', 'aagan' ),
				'info'    => esc_html__('YES! to show post views information', 'aagan'),
				'default' => true
			),

		),
	),

	// -----------------------------------------
	// 404 Options
	// -----------------------------------------
	array(
	  'name'      => '404_options',
	  'title'     => esc_html__('404 Options', 'aagan'),
	  'icon'      => 'fa fa-warning',

		'fields'      => array(

		  array(
			'type'    => 'subheading',
			'content' => esc_html__( "404 Message", 'aagan' ),
		  ),
		  
		  array(
			'id'      => 'enable-404message',
			'type'    => 'switcher',
			'title'   => esc_html__('Enable Message', 'aagan' ),
			'info'	  => esc_html__('YES! to enable not-found page message.', 'aagan'),
			'default' => true
		  ),

		  array(
			'id'           => 'notfound-style',
			'type'         => 'select',
			'title'        => esc_html__('Template Style', 'aagan'),
			'options'      => array(
			  'type1' 	   => esc_html__('Modern', 'aagan'),
			  'type2'      => esc_html__('Classic', 'aagan'),
			  'type4'  	   => esc_html__('Diamond', 'aagan'),
			  'type5'      => esc_html__('Shadow', 'aagan'),
			  'type6'      => esc_html__('Diamond Alt', 'aagan'),
			  'type7'  	   => esc_html__('Stack', 'aagan'),
			  'type8'  	   => esc_html__('Minimal', 'aagan'),
			),
			'class'        => 'chosen',
			'default'      => 'type2',
			'info'         => esc_html__('Choose the style of not-found template page.', 'aagan')
		  ),

		  array(
			'id'      => 'notfound-darkbg',
			'type'    => 'switcher',
			'title'   => esc_html__('404 Dark BG', 'aagan' ),
			'info'	  => esc_html__('YES! to use dark bg notfound page for this site.', 'aagan'),
			'default' => true
		  ),

		  array(
			'id'           => 'notfound-pageid',
			'type'         => 'select',
			'title'        => esc_html__('Custom Page', 'aagan'),
			'options'      => 'pages',
			'class'        => 'chosen',
			'default_option' => esc_html__('Choose the page', 'aagan'),
			'info'       	 => esc_html__('Choose the page for not-found content.', 'aagan')
		  ),
		  
		  array(
			'type'    => 'subheading',
			'content' => esc_html__( "Background Options", 'aagan' ),
		  ),

		  array(
			'id'    => 'notfound_background',
			'type'  => 'background',
			'title' => esc_html__('Background', 'aagan'),
			'default' => array (
				'image' => AAGAN_THEME_URI . '/images/404.jpg',
			  ),
		  ),

		  array(
			'id'  		 => 'notfound-bg-style',
			'type'  	 => 'textarea',
			'title' 	 => esc_html__('Custom Styles', 'aagan'),
			'info'		 => esc_html__('Paste custom CSS styles for not found page.', 'aagan')
		  ),

		),
	),

	// -----------------------------------------
	// Underconstruction Options
	// -----------------------------------------
	array(
	  'name'      => 'comingsoon_options',
	  'title'     => esc_html__('Under Construction Options', 'aagan'),
	  'icon'      => 'fa fa-thumbs-down',

		'fields'      => array(

		  array(
			'type'    => 'subheading',
			'content' => esc_html__( "Under Construction", 'aagan' ),
		  ),
	
		  array(
			'id'      => 'enable-comingsoon',
			'type'    => 'switcher',
			'title'   => esc_html__('Enable Coming Soon', 'aagan' ),
			'info'	  => esc_html__('YES! to check under construction page of your website.', 'aagan')
		  ),
	
		  array(
			'id'           => 'comingsoon-style',
			'type'         => 'select',
			'title'        => esc_html__('Template Style', 'aagan'),
			'options'      => array(
			  'type1' 	   => esc_html__('Diamond', 'aagan'),
			  'type2'      => esc_html__('Teaser', 'aagan'),
			  'type3'  	   => esc_html__('Minimal', 'aagan'),
			  'type4'      => esc_html__('Counter Only', 'aagan'),
			  'type5'      => esc_html__('Belt', 'aagan'),
			  'type6'  	   => esc_html__('Classic', 'aagan'),
			  'type7'  	   => esc_html__('Boxed', 'aagan')
			),
			'class'        => 'chosen',
			'default'      => 'type1',
			'info'         => esc_html__('Choose the style of coming soon template.', 'aagan'),
		  ),

		  array(
			'id'      => 'uc-darkbg',
			'type'    => 'switcher',
			'title'   => esc_html__('Coming Soon Dark BG', 'aagan' ),
			'info'	  => esc_html__('YES! to use dark bg coming soon page for this site.', 'aagan'),
			'default' => true
		  ),

		  array(
			'id'           => 'comingsoon-pageid',
			'type'         => 'select',
			'title'        => esc_html__('Custom Page', 'aagan'),
			'options'      => 'pages',
			'class'        => 'chosen',
			'default_option' => esc_html__('Choose the page', 'aagan'),
			'info'       	 => esc_html__('Choose the page for comingsoon content.', 'aagan')
		  ),

		  array(
			'id'      => 'show-launchdate',
			'type'    => 'switcher',
			'title'   => esc_html__('Show Launch Date', 'aagan' ),
			'info'	  => esc_html__('YES! to show launch date text.', 'aagan'),
			'default' => true
		  ),

		  array(
			'id'      => 'comingsoon-launchdate',
			'type'    => 'text',
			'title'   => esc_html__('Launch Date', 'aagan'),
			'attributes' => array( 
			  'placeholder' => '10/30/2016 12:00:00'
			),
			'default' => '10/30/2020 12:00:00',
			'after' 	=> '<p class="cs-text-info">'.esc_html__('Put Format: 12/30/2016 12:00:00 month/day/year hour:minute:second', 'aagan').'</p>',
		  ),

		  array(
			'id'           => 'comingsoon-timezone',
			'type'         => 'select',
			'title'        => esc_html__('UTC Timezone', 'aagan'),
			'options'      => array(
			  '-12' => '-12', '-11' => '-11', '-10' => '-10', '-9' => '-9', '-8' => '-8', '-7' => '-7', '-6' => '-6', '-5' => '-5', 
			  '-4' => '-4', '-3' => '-3', '-2' => '-2', '-1' => '-1', '0' => '0', '+1' => '+1', '+2' => '+2', '+3' => '+3', '+4' => '+4',
			  '+5' => '+5', '+6' => '+6', '+7' => '+7', '+8' => '+8', '+9' => '+9', '+10' => '+10', '+11' => '+11', '+12' => '+12'
			),
			'class'        => 'chosen',
			'default'      => '0',
			'info'         => esc_html__('Choose utc timezone, by default UTC:00:00', 'aagan'),
		  ),

		  array(
			'id'    => 'comingsoon_background',
			'type'  => 'background',
			'title' => esc_html__('Background', 'aagan'),
			'default' => array (
				'image' => AAGAN_THEME_URI . '/images/coming-soon.jpg',
			  ),
		  ),

		  array(
			'id'  		 => 'comingsoon-bg-style',
			'type'  	 => 'textarea',
			'title' 	 => esc_html__('Custom Styles', 'aagan'),
			'info'		 => esc_html__('Paste custom CSS styles for under construction page.', 'aagan'),
		  ),

		),
	),

  ),
);

// -----------------------------------------
// Widget area Options
// -----------------------------------------
$options[]      = array(
  'name'        => 'widgetarea_options',
  'title'       => esc_html__('Widget Area', 'aagan'),
  'icon'        => 'fa fa-trello',

  'fields'      => array(

	  array(
		'type'    => 'subheading',
		'content' => esc_html__( "Custom Widget Area for Sidebar", 'aagan' ),
	  ),

	  array(
		'id'           => 'wtitle-style',
		'type'         => 'select',
		'title'        => esc_html__('Sidebar widget Title Style', 'aagan'),
		'options'      => array(
		  'default' => esc_html__('Choose any type', 'aagan'),
		  'type1' 	   => esc_html__('Double Border', 'aagan'),
		  'type2'      => esc_html__('Tooltip', 'aagan'),
		  'type3'  	   => esc_html__('Title Top Border', 'aagan'),
		  'type4'      => esc_html__('Left Border & Pattren', 'aagan'),
		  'type5'      => esc_html__('Bottom Border', 'aagan'),
		  'type6'  	   => esc_html__('Tooltip Border', 'aagan'),
		  'type7'  	   => esc_html__('Boxed Modern', 'aagan'),
		  'type8'  	   => esc_html__('Elegant Border', 'aagan'),
		  'type9' 	   => esc_html__('Needle', 'aagan'),
		  'type10' 	   => esc_html__('Ribbon', 'aagan'),
		  'type11' 	   => esc_html__('Content Background', 'aagan'),
		  'type12' 	   => esc_html__('Classic BG', 'aagan'),
		  'type13' 	   => esc_html__('Tiny Boders', 'aagan'),
		  'type14' 	   => esc_html__('BG & Border', 'aagan'),
		  'type15' 	   => esc_html__('Classic BG Alt', 'aagan'),
		  'type16' 	   => esc_html__('Left Border & BG', 'aagan'),
		  'type17' 	   => esc_html__('Basic', 'aagan'),
		  'type18' 	   => esc_html__('BG & Pattern', 'aagan'),
		),
		'class'          => 'chosen',
		'default' 		 =>  'type16',
		'info'           => esc_html__('Choose the style of sidebar widget title.', 'aagan')
	  ),

	  array(
		'id'              => 'widgetarea-custom',
		'type'            => 'group',
		'title'           => esc_html__('Custom Widget Area', 'aagan'),
		'button_title'    => esc_html__('Add New', 'aagan'),
		'accordion_title' => esc_html__('Add New Widget Area', 'aagan'),
		'fields'          => array(

		  array(
			'id'          => 'widgetarea-custom-name',
			'type'        => 'text',
			'title'       => esc_html__('Name', 'aagan'),
		  ),

		)
	  ),

	),
);

// -----------------------------------------
// Woocommerce Options
// -----------------------------------------
if( function_exists( 'is_woocommerce' ) && ! class_exists ( 'DTWooPlugin' ) ){

	$options[]      = array(
	  'name'        => 'woocommerce_options',
	  'title'       => esc_html__('Woocommerce', 'aagan'),
	  'icon'        => 'fa fa-shopping-cart',

	  'fields'      => array(

		  array(
			'type'    => 'subheading',
			'content' => esc_html__( "Woocommerce Shop Page Options", 'aagan' ),
		  ),

		  array(
			'id'  		 => 'shop-product-per-page',
			'type'  	 => 'number',
			'title' 	 => esc_html__('Products Per Page', 'aagan'),
			'after'		 => '<span class="cs-text-desc">&nbsp;'.esc_html__('Number of products to show in catalog / shop page', 'aagan').'</span>',
			'default' 	 => 12,
		  ),

		  array(
			'id'           => 'product-style',
			'type'         => 'select',
			'title'        => esc_html__('Product Style', 'aagan'),
			'options'      => array(
			  'woo-type1' 	   => esc_html__('Thick Border', 'aagan'),
			  'woo-type4'      => esc_html__('Diamond Icons', 'aagan'),
			  'woo-type8' 	   => esc_html__('Modern', 'aagan'),
			  'woo-type10' 	   => esc_html__('Easing', 'aagan'),
			  'woo-type11' 	   => esc_html__('Boxed', 'aagan'),
			  'woo-type12' 	   => esc_html__('Easing Alt', 'aagan'),
			  'woo-type13' 	   => esc_html__('Parallel', 'aagan'),
			  'woo-type14' 	   => esc_html__('Pointer', 'aagan'),
			  'woo-type16' 	   => esc_html__('Stack', 'aagan'),
			  'woo-type17' 	   => esc_html__('Bouncy', 'aagan'),
			  'woo-type20' 	   => esc_html__('Masked Circle', 'aagan'),
			  'woo-type21' 	   => esc_html__('Classic', 'aagan')
			),
			'class'        => 'chosen',
			'default' 	   => 'woo-type1',
			'info'         => esc_html__('Choose products style to display shop & archive pages.', 'aagan')
		  ),

		  array(
			'id'      	 => 'shop-page-product-layout',
			'type'       => 'image_select',
			'title'      => esc_html__('Product Layout', 'aagan'),
			'options'    => array(
				  1   => AAGAN_THEME_URI . '/cs-framework-override/images/one-column.png',
				  2   => AAGAN_THEME_URI . '/cs-framework-override/images/one-half-column.png',
				  3   => AAGAN_THEME_URI . '/cs-framework-override/images/one-third-column.png',
				  4   => AAGAN_THEME_URI . '/cs-framework-override/images/one-fourth-column.png',
			),
			'default'      => 4,
			'attributes'   => array(
			  'data-depend-id' => 'shop-page-product-layout',
			),
		  ),

		  array(
			'type'    => 'subheading',
			'content' => esc_html__( "Product Detail Page Options", 'aagan' ),
		  ),

		  array(
			'id'      	   => 'product-layout',
			'type'         => 'image_select',
			'title'        => esc_html__('Layout', 'aagan'),
			'options'      => array(
			  'content-full-width'   => AAGAN_THEME_URI . '/cs-framework-override/images/without-sidebar.png',
			  'with-left-sidebar'    => AAGAN_THEME_URI . '/cs-framework-override/images/left-sidebar.png',
			  'with-right-sidebar'   => AAGAN_THEME_URI . '/cs-framework-override/images/right-sidebar.png',
			  'with-both-sidebar'    => AAGAN_THEME_URI . '/cs-framework-override/images/both-sidebar.png',
			),
			'default'      => 'content-full-width',
			'attributes'   => array(
			  'data-depend-id' => 'product-layout',
			),
		  ),

		  array(
			'id'  		 	 => 'show-shop-standard-left-sidebar-for-product-layout',
			'type'  		 => 'switcher',
			'title' 		 => esc_html__('Show Shop Standard Left Sidebar', 'aagan'),
			'dependency'   	 => array( 'product-layout', 'any', 'with-left-sidebar,with-both-sidebar' ),
		  ),

		  array(
			'id'  			 => 'show-shop-standard-right-sidebar-for-product-layout',
			'type'  		 => 'switcher',
			'title' 		 => esc_html__('Show Shop Standard Right Sidebar', 'aagan'),
			'dependency' 	 => array( 'product-layout', 'any', 'with-right-sidebar,with-both-sidebar' ),
		  ),

		  array(
			'id'  		 	 => 'enable-related',
			'type'  		 => 'switcher',
			'title' 		 => esc_html__('Show Related Products', 'aagan'),
			'info'	  		 => esc_html__("YES! to display related products on single product's page.", 'aagan')
		  ),

		  array(
			'type'    => 'subheading',
			'content' => esc_html__( "Product Category Page Options", 'aagan' ),
		  ),

		  array(
			'id'      	   => 'product-category-layout',
			'type'         => 'image_select',
			'title'        => esc_html__('Layout', 'aagan'),
			'options'      => array(
			  'content-full-width'   => AAGAN_THEME_URI . '/cs-framework-override/images/without-sidebar.png',
			  'with-left-sidebar'    => AAGAN_THEME_URI . '/cs-framework-override/images/left-sidebar.png',
			  'with-right-sidebar'   => AAGAN_THEME_URI . '/cs-framework-override/images/right-sidebar.png',
			  'with-both-sidebar'    => AAGAN_THEME_URI . '/cs-framework-override/images/both-sidebar.png',
			),
			'default'      => 'content-full-width',
			'attributes'   => array(
			  'data-depend-id' => 'product-category-layout',
			),
		  ),

		  array(
			'id'  		 	 => 'show-shop-standard-left-sidebar-for-product-category-layout',
			'type'  		 => 'switcher',
			'title' 		 => esc_html__('Show Shop Standard Left Sidebar', 'aagan'),
			'dependency'   	 => array( 'product-category-layout', 'any', 'with-left-sidebar,with-both-sidebar' ),
		  ),

		  array(
			'id'  			 => 'show-shop-standard-right-sidebar-for-product-category-layout',
			'type'  		 => 'switcher',
			'title' 		 => esc_html__('Show Shop Standard Right Sidebar', 'aagan'),
			'dependency' 	 => array( 'product-category-layout', 'any', 'with-right-sidebar,with-both-sidebar' ),
		  ),
		  
		  array(
			'type'    => 'subheading',
			'content' => esc_html__( "Product Tag Page Options", 'aagan' ),
		  ),

		  array(
			'id'      	   => 'product-tag-layout',
			'type'         => 'image_select',
			'title'        => esc_html__('Layout', 'aagan'),
			'options'      => array(
			  'content-full-width'   => AAGAN_THEME_URI . '/cs-framework-override/images/without-sidebar.png',
			  'with-left-sidebar'    => AAGAN_THEME_URI . '/cs-framework-override/images/left-sidebar.png',
			  'with-right-sidebar'   => AAGAN_THEME_URI . '/cs-framework-override/images/right-sidebar.png',
			  'with-both-sidebar'    => AAGAN_THEME_URI . '/cs-framework-override/images/both-sidebar.png',
			),
			'default'      => 'content-full-width',
			'attributes'   => array(
			  'data-depend-id' => 'product-tag-layout',
			),
		  ),

		  array(
			'id'  		 	 => 'show-shop-standard-left-sidebar-for-product-tag-layout',
			'type'  		 => 'switcher',
			'title' 		 => esc_html__('Show Shop Standard Left Sidebar', 'aagan'),
			'dependency'   	 => array( 'product-tag-layout', 'any', 'with-left-sidebar,with-both-sidebar' ),
		  ),

		  array(
			'id'  			 => 'show-shop-standard-right-sidebar-for-product-tag-layout',
			'type'  		 => 'switcher',
			'title' 		 => esc_html__('Show Shop Standard Right Sidebar', 'aagan'),
			'dependency' 	 => array( 'product-tag-layout', 'any', 'with-right-sidebar,with-both-sidebar' ),
		  ),

	  ),
	);
}

// -----------------------------------------
// Sociable Options
// -----------------------------------------
$options[]      = array(
  'name'        => 'sociable_options',
  'title'       => esc_html__('Sociable', 'aagan'),
  'icon'        => 'fa fa-share-alt-square',

  'fields'      => array(

	  array(
		'type'    => 'subheading',
		'content' => esc_html__( "Sociable", 'aagan' ),
	  ),

	  array(
		'id'              => 'sociable_fields',
		'type'            => 'group',
		'title'           => esc_html__('Sociable', 'aagan'),
		'info'            => esc_html__('Click button to add type of social & url.', 'aagan'),
		'button_title'    => esc_html__('Add New Social', 'aagan'),
		'accordion_title' => esc_html__('Adding New Social Field', 'aagan'),
		'fields'          => array(
		  array(
			'id'          => 'sociable_fields_type',
			'type'        => 'select',
			'title'       => esc_html__('Select Social', 'aagan'),
			'options'      => array(
			  'delicious' 	 => esc_html__('Delicious', 'aagan'),
			  'deviantart' 	 => esc_html__('Deviantart', 'aagan'),
			  'digg' 	  	 => esc_html__('Digg', 'aagan'),
			  'dribbble' 	 => esc_html__('Dribbble', 'aagan'),
			  'envelope' 	 => esc_html__('Envelope', 'aagan'),
			  'facebook' 	 => esc_html__('Facebook', 'aagan'),
			  'flickr' 		 => esc_html__('Flickr', 'aagan'),
			  'google-plus'  => esc_html__('Google Plus', 'aagan'),
			  'gtalk'  		 => esc_html__('GTalk', 'aagan'),
			  'instagram'	 => esc_html__('Instagram', 'aagan'),
			  'lastfm'	 	 => esc_html__('Lastfm', 'aagan'),
			  'linkedin'	 => esc_html__('Linkedin', 'aagan'),
			  'myspace'		 => esc_html__('Myspace', 'aagan'),
			  'picasa'		 => esc_html__('Picasa', 'aagan'),
			  'pinterest'	 => esc_html__('Pinterest', 'aagan'),
			  'reddit'		 => esc_html__('Reddit', 'aagan'),
			  'rss'		 	 => esc_html__('RSS', 'aagan'),
			  'skype'		 => esc_html__('Skype', 'aagan'),
			  'stumbleupon'	 => esc_html__('Stumbleupon', 'aagan'),
			  'technorati'	 => esc_html__('Technorati', 'aagan'),
			  'tumblr'		 => esc_html__('Tumblr', 'aagan'),
			  'twitter'		 => esc_html__('Twitter', 'aagan'),
			  'viadeo'		 => esc_html__('Viadeo', 'aagan'),
			  'vimeo'		 => esc_html__('Vimeo', 'aagan'),
			  'yahoo'		 => esc_html__('Yahoo', 'aagan'),
			  'youtube'		 => esc_html__('Youtube', 'aagan'),
			),
			'class'        => 'chosen',
			'default'      => 'delicious',
		  ),

		  array(
			'id'          => 'sociable_fields_url',
			'type'        => 'text',
			'title'       => esc_html__('Enter URL', 'aagan')
		  ),
		)
	  ),

   ),
);

// -----------------------------------------
// Hook Options
// -----------------------------------------
$options[]      = array(
  'name'        => 'hook_options',
  'title'       => esc_html__('Hooks', 'aagan'),
  'icon'        => 'fa fa-paperclip',

  'fields'      => array(

	  array(
		'type'    => 'subheading',
		'content' => esc_html__( "Top Hook", 'aagan' ),
	  ),

	  array(
		'id'  	=> 'enable-top-hook',
		'type'  => 'switcher',
		'title' => esc_html__('Enable Top Hook', 'aagan'),
		'info'	=> esc_html__("YES! to enable top hook.", 'aagan')
	  ),

	  array(
		'id'  		 => 'top-hook',
		'type'  	 => 'textarea',
		'title' 	 => esc_html__('Top Hook', 'aagan'),
		'info'		 => esc_html__('Paste your top hook, Executes after the opening &lt;body&gt; tag.', 'aagan')
	  ),

	  array(
		'type'    => 'subheading',
		'content' => esc_html__( "Content Before Hook", 'aagan' ),
	  ),

	  array(
		'id'  	=> 'enable-content-before-hook',
		'type'  => 'switcher',
		'title' => esc_html__('Enable Content Before Hook', 'aagan'),
		'info'	=> esc_html__("YES! to enable content before hook.", 'aagan')
	  ),

	  array(
		'id'  		 => 'content-before-hook',
		'type'  	 => 'textarea',
		'title' 	 => esc_html__('Content Before Hook', 'aagan'),
		'info'		 => esc_html__('Paste your content before hook, Executes before the opening &lt;#primary&gt; tag.', 'aagan')
	  ),

	  array(
		'type'    => 'subheading',
		'content' => esc_html__( "Content After Hook", 'aagan' ),
	  ),

	  array(
		'id'  	=> 'enable-content-after-hook',
		'type'  => 'switcher',
		'title' => esc_html__('Enable Content After Hook', 'aagan'),
		'info'	=> esc_html__("YES! to enable content after hook.", 'aagan')
	  ),

	  array(
		'id'  		 => 'content-after-hook',
		'type'  	 => 'textarea',
		'title' 	 => esc_html__('Content After Hook', 'aagan'),
		'info'		 => esc_html__('Paste your content after hook, Executes after the closing &lt;/#main&gt; tag.', 'aagan')
	  ),

	  array(
		'type'    => 'subheading',
		'content' => esc_html__( "Bottom Hook", 'aagan' ),
	  ),

	  array(
		'id'  	=> 'enable-bottom-hook',
		'type'  => 'switcher',
		'title' => esc_html__('Enable Bottom Hook', 'aagan'),
		'info'	=> esc_html__("YES! to enable bottom hook.", 'aagan')
	  ),

	  array(
		'id'  		 => 'bottom-hook',
		'type'  	 => 'textarea',
		'title' 	 => esc_html__('Bottom Hook', 'aagan'),
		'info'		 => esc_html__('Paste your bottom hook, Executes after the closing &lt;/body&gt; tag.', 'aagan')
	  ),

	  array(
		'id'  	=> 'enable-analytics-code',
		'type'  => 'switcher',
		'title' => esc_html__('Enable Tracking Code', 'aagan'),
		'info'	=> esc_html__("YES! to enable site tracking code.", 'aagan')
	  ),

	  array(
		'id'  		 => 'analytics-code',
		'type'  	 => 'textarea',
		'title' 	 => esc_html__('Google Analytics Tracking Code', 'aagan'),
		'info'		 => esc_html__('Either enter your Google tracking id (UA-XXXXX-X) or your full Google Analytics tracking Code here. If you want to offer your visitors the option to stop being tracked you can place the shortcode [dt_sc_privacy_google_tracking] somewhere on your site', 'aagan')
	  ),

   ),
);

// -----------------------------------------
// Custom Font Options
// -----------------------------------------
$options[]      = array(
  'name'        => 'font_options',
  'title'       => esc_html__('Custom Fonts', 'aagan'),
  'icon'        => 'fa fa-font',

  'fields'      => array(

	  array(
		'type'    => 'subheading',
		'content' => esc_html__( "Custom Fonts", 'aagan' ),
	  ),

	  array(
		'id'              => 'custom_font_fields',
		'type'            => 'group',
		'title'           => esc_html__('Custom Font', 'aagan'),
		'info'            => esc_html__('Click button to add font name & urls.', 'aagan'),
		'button_title'    => esc_html__('Add New Font', 'aagan'),
		'accordion_title' => esc_html__('Adding New Font Field', 'aagan'),
		'fields'          => array(
		  array(
			'id'          => 'custom_font_name',
			'type'        => 'text',
			'title'       => esc_html__('Font Name', 'aagan')
		  ),

		  array(
			'id'      => 'custom_font_woof',
			'type'    => 'upload',
			'title'   => esc_html__('Upload File (*.woff)', 'aagan'),
			'after'   => '<p class="cs-text-muted">'.esc_html__('You can upload custom font family (*.woff) file here.', 'aagan').'</p>',
		  ),

		  array(
			'id'      => 'custom_font_woof2',
			'type'    => 'upload',
			'title'   => esc_html__('Upload File (*.woff2)', 'aagan'),
			'after'   => '<p class="cs-text-muted">'.esc_html__('You can upload custom font family (*.woff2) file here.', 'aagan').'</p>',
		  )
		)
	  ),

   ),
);

// ------------------------------
// backup                       
// ------------------------------
$options[]   = array(
  'name'     => 'backup_section',
  'title'    => esc_html__('Backup', 'aagan'),
  'icon'     => 'fa fa-shield',
  'fields'   => array(

    array(
      'type'    => 'notice',
      'class'   => 'warning',
      'content' => esc_html__('You can save your current options. Download a Backup and Import.', 'aagan')
    ),

    array(
      'type'    => 'backup',
    ),

  )
);

// ------------------------------
// license
// ------------------------------
$options[]   = array(
  'name'     => 'theme_version',
  'title'    => constant('AAGAN_THEME_NAME').esc_html__(' Log', 'aagan'),
  'icon'     => 'fa fa-info-circle',
  'fields'   => array(

    array(
      'type'    => 'heading',
      'content' => constant('AAGAN_THEME_NAME').esc_html__(' Theme Change Log', 'aagan')
    ),
    array(
      'type'    => 'content',
			'content' => '<pre>

2020.06.12 - version 2.2
* Compatible with wordpress 5.4.2
* Updated: Activating another theme causes error
* Updated: Breadcrumb parallax issue
* Updated: Social Icons Shortcode design issue
* Updated: Testimonial - content area - bold option doesnt work
* Updated: Responsive issue in map
* Updated: Demo content error

2020.02.06 - version 2.1

* Updated : All premium plugins			
			
2020.01.29 - version 2.0

* Compatible with wordpress 5.3.2
* Updated: All premium plugins
* Updated: All wordpress theme standards
* Updated: Privacy and Cookies concept
* Updated: Gutenberg editor support for custom post types

* Fixed: Google Analytics issue
* Fixed: Mailchimp email client issue
* Fixed: Privacy Button Issue
* Fixed: Gutenberg check for old wordpress version

* Improved: Tags taxonomy added for portfolio
* Improved: Single product breadcrumb section
* Improved: Revisions options added for all custom posts

2019.11.14 - version 1.9
* Updated all wordpress theme standards
* Compatible with latest Gutenberg editor
* Updated: All premium plugins
* Compatible with wordpress 5.2.4

2019.08.23 - version 1.8
* Compatible with wordpress 5.2.2
* Updated: All premium plugins
* Updated: Revisions added to all custom post types
* Updated: Gutenberg editor support for custom post types
* Updated: Link for phone number module
* Updated: Online documentation link, check readme file

* Fixed: Customizer logo option
* Fixed: Google Analytics issue
* Fixed: Mailchimp email client issue
* Fixed: Gutenberg check for old wordpress version
* Fixed: Edit with Visual Composer for portfolio
* Fixed: Header & Footer wpml option
* Fixed: Site title color
* Fixed: Privacy popup bg color
* Fixed: 404 page scrolling issue

* Improved: Single product breadcrumb section
* Improved: Tags taxonomy added for portfolio
* Improved: Woocommerce cart module added with custom class option

* New: Whatsapp Shortcode

2019.05.20 - version 1.7
* Gutenberg Latest fixes
* Updated Visual Composer and Layerslider plugins
			
2019.03.30 - version 1.6
 * Gutenberg Latest update compatible
 * Portfolio Video option
 * Coming Soon page fix
 * Portfolio archive page breadcrumb fix
 * Mega menu image fix
 * GDPR product single page fix
 * Codestar framework update
 * Wpml xml file updated
 * disable options for likes and views in single post page
 * Updated latest version of all third party plugins
 * Some design tweaks
 * 
2019.01.19 - version 1.5
 * Gutenberg compatible
 * Latest WordPress version 5.0.3 compatible
 * Updated latest version of all third party plugins
 * Some design tweaks
       
2018.10.29 - version 1.4
 * Gutenberg plugin compatible
 * Updated latest version of all third party plugins
 * Updated documentation
 
2018.07.10 - version 1.3
 * GDPR Compliant update in comment form, mailchimp form etc.
 * Additional 8 new home pages added
 * Few shortcodes design issues fixed
 * Unyson Demo Content files updated
 * Updated latest version of all third party plugins

2018.06.11 - version 1.2
 * Fix - Unyson Demo Content import issue 
 * Fix - Bulk plugin installation error

2018.05.06 - version 1.1
 * Unyson Demo Content Updated

2018.05.31 - version 1.0
 * First release!  </pre>',
    ),

  )
);

// ------------------------------
// Seperator
// ------------------------------
$options[] = array(
  'name'   => 'seperator_1',
  'title'  => esc_html__('Plugin Options', 'aagan'),
  'icon'   => 'fa fa-plug'
);


CSFramework::instance( $settings, $options );