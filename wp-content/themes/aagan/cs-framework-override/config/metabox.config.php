<?php if ( ! defined( 'ABSPATH' ) ) { die; } // Cannot access pages directly.

// -----------------------------------------
// Custom Widgets                    -
// -----------------------------------------
function aagan_custom_widgets() {
  $custom_widgets = array();
  $widgets = is_array( cs_get_option( 'widgetarea-custom' ) ) ? cs_get_option( 'widgetarea-custom' ) : array();
  $widgets = array_filter($widgets);

  if( isset( $widgets ) ):
    foreach ( $widgets as $widget ) :
      $id = mb_convert_case($widget['widgetarea-custom-name'], MB_CASE_LOWER, "UTF-8");
      $id = str_replace(" ", "-", $id);
      $custom_widgets[$id] = $widget['widgetarea-custom-name'];
    endforeach;
  endif;

  return $custom_widgets;
}

// -----------------------------------------
// Layer Sliders
// -----------------------------------------
function aagan_layersliders() {
  $layerslider = array(  esc_html__('Select a slider','aagan') );

  if( class_exists( 'LS_Sliders' ) ) {

    $sliders = LS_Sliders::find(array('limit' => 50));

    if(!empty($sliders)) {
      foreach($sliders as $key => $item){
        $layerslider[ $item['id'] ] = $item['name'];
      }
    }
  }

  return $layerslider;
}

// -----------------------------------------
// Revolution Sliders
// -----------------------------------------
function aagan_revolutionsliders() {
  $revolutionslider = array( '' => esc_html__('Select a slider','aagan') );

  if(class_exists( 'RevSlider' )) {
    $sld = new RevSlider();
    $sliders = $sld->getArrSliders();
    if(!empty($sliders)){
      foreach($sliders as $key => $item) {
        $revolutionslider[$item->getAlias()] = $item->getTitle();
      }
    }    
  }

  return $revolutionslider;  
}

// -----------------------------------------
// Meta Layout Section
// -----------------------------------------
$meta_layout_section =array(
  'name'  => 'layout_section',
  'title' => esc_html__('Layout', 'aagan'),
  'icon'  => 'fa fa-columns',
  'fields' =>  array(
    array(
      'id'  => 'layout',
      'type' => 'image_select',
      'title' => esc_html__('Page Layout', 'aagan' ),
      'options'      => array(
          'content-full-width'   => AAGAN_THEME_URI . '/cs-framework-override/images/without-sidebar.png',
          'with-left-sidebar'    => AAGAN_THEME_URI . '/cs-framework-override/images/left-sidebar.png',
          'with-right-sidebar'   => AAGAN_THEME_URI . '/cs-framework-override/images/right-sidebar.png',
          'with-both-sidebar'    => AAGAN_THEME_URI . '/cs-framework-override/images/both-sidebar.png',
          'fullwidth'            => AAGAN_THEME_URI . '/cs-framework-override/images/fullwidth.png',
      ),
      'default'      => 'content-full-width',
	  'info'		 => esc_html__('Layout "fullwidth" only apply for portfolio template.', 'aagan'),
      'attributes'   => array( 'data-depend-id' => 'page-layout' )
    ),
    array(
      'id'        => 'show-standard-sidebar-left',
      'type'      => 'switcher',
      'title'     => esc_html__('Show Standard Left Sidebar', 'aagan' ),
      'dependency'  => array( 'page-layout', 'any', 'with-left-sidebar,with-both-sidebar' ),
    ),
    array(
      'id'        => 'widget-area-left',
      'type'      => 'select',
      'title'     => esc_html__('Choose Left Widget Areas', 'aagan' ),
      'class'     => 'chosen',
      'options'   => aagan_custom_widgets(),
      'attributes'  => array( 
        'multiple'  => 'multiple',
        'data-placeholder' => esc_html__('Select Left Widget Areas','aagan'),
        'style' => 'width: 400px;'
      ),
      'dependency'  => array( 'page-layout', 'any', 'with-left-sidebar,with-both-sidebar' ),
    ),
    array(
      'id'          => 'show-standard-sidebar-right',
      'type'        => 'switcher',
      'title'       => esc_html__('Show Standard Right Sidebar', 'aagan' ),
      'dependency'  => array( 'page-layout', 'any', 'with-right-sidebar,with-both-sidebar' ),
    ),
    array(
      'id'        => 'widget-area-right',
      'type'      => 'select',
      'title'     => esc_html__('Choose Right Widget Areas', 'aagan' ),
      'class'     => 'chosen',
      'options'   => aagan_custom_widgets(),
      'attributes'    => array( 
        'multiple' => 'multiple',
        'data-placeholder' => esc_html__('Select Right Widget Areas','aagan'),
        'style' => 'width: 400px;'
      ),
      'dependency'  => array( 'page-layout', 'any', 'with-right-sidebar,with-both-sidebar' ),
    )
  )
);

// -----------------------------------------
// Meta Breadcrumb Section
// -----------------------------------------
$meta_breadcrumb_section = array(
  'name'  => 'breadcrumb_section',
  'title' => esc_html__('Breadcrumb', 'aagan'),
  'icon'  => 'fa fa-arrows-h',
  'fields' =>  array(
    array(
      'id'      => 'enable-sub-title',
      'type'    => 'switcher',
      'title'   => esc_html__('Show Breadcrumb', 'aagan' ),
      'default' => false
    ),
    array(
    	'id'                 => 'breadcrumb_position',
	'type'               => 'select',
      'title'              => esc_html__('Position', 'aagan' ),
      'options'            => array(
      'header-top-absolute'    => esc_html__('Behind the Header','aagan'),
      'header-top-relative' 	   => esc_html__('Default','aagan'),
		),
		'default'            => 'header-top-absolute',	
      'dependency'         => array( 'enable-sub-title', '==', 'true' ),
    ),    
    array(
      'id'    => 'breadcrumb_background',
      'type'  => 'background',
      'title' => esc_html__('Background', 'aagan' ),
      'dependency'   => array( 'enable-sub-title', '==', 'true' ),
      'default' => array (
        'image'       => AAGAN_THEME_URI . '/images/breadcrumb-bgimage.jpg',
        'size'        => 'cover',
        'repeat'   	  => 'no-repeat',
        'attachment'  => 'fixed',
        'position'    => 'center bottom',
        'color'	   	  => 'rgba(0,0,0,0.85)',
      ),
    ),
  )
);

// -----------------------------------------
// Meta Slider Section
// -----------------------------------------
$meta_slider_section = array(
  'name'  => 'slider_section',
  'title' => esc_html__('Slider', 'aagan'),
  'icon'  => 'fa fa-slideshare',
  'fields' =>  array(
    array(
      'id'           => 'slider-notice',
      'type'         => 'notice',
      'class'        => 'danger',
      'content'      => esc_html__('Slider tab works only if breadcrumb disabled.','aagan'),
      'class'        => 'margin-30 cs-danger',
      'dependency'   => array( 'enable-sub-title', '==', 'true' ),
    ),

    array(
      'id'           => 'show_slider',
      'type'         => 'switcher',
      'title'        => esc_html__('Show Slider', 'aagan' ),
      'dependency'   => array( 'enable-sub-title', '==', 'false' ),
    ),
    array(
    	'id'                 => 'slider_position',
	'type'               => 'select',
	'title'              => esc_html__('Position', 'aagan' ),
	'options'            => array(
		'header-top-relative'     => esc_html__('Top Header Relative','aagan'),
		'header-top-absolute'    => esc_html__('Top Header Absolute','aagan'),
		'bottom-header' 	   => esc_html__('Bottom Header','aagan'),
	),
	'default'            => 'bottom-header',
	'dependency'         => array( 'enable-sub-title|show_slider', '==|==', 'false|true' ),
   ),
   array(
      'id'                 => 'slider_type',
      'type'               => 'select',
      'title'              => esc_html__('Slider', 'aagan' ),
      'options'            => array(
        ''                 => esc_html__('Select a slider','aagan'),
        'layerslider'      => esc_html__('Layer slider','aagan'),
        'revolutionslider' => esc_html__('Revolution slider','aagan'),
        'customslider'     => esc_html__('Custom Slider Shortcode','aagan'),
      ),
      'validate' => 'required',
      'dependency'         => array( 'enable-sub-title|show_slider', '==|==', 'false|true' ),
    ),

    array(
      'id'          => 'layerslider_id',
      'type'        => 'select',
      'title'       => esc_html__('Layer Slider', 'aagan' ),
      'options'     => aagan_layersliders(),
      'validate'    => 'required',
      'dependency'  => array( 'enable-sub-title|show_slider|slider_type', '==|==|==', 'false|true|layerslider' )
    ),

    array(
      'id'          => 'revolutionslider_id',
      'type'        => 'select',
      'title'       => esc_html__('Revolution Slider', 'aagan' ),
      'options'     => aagan_revolutionsliders(),
      'validate'    => 'required',
      'dependency'  => array( 'enable-sub-title|show_slider|slider_type', '==|==|==', 'false|true|revolutionslider' )
    ),

    array(
      'id'          => 'customslider_sc',
      'type'        => 'textarea',
      'title'       => esc_html__('Custom Slider Code', 'aagan' ),
      'validate'    => 'required',
      'dependency'  => array( 'enable-sub-title|show_slider|slider_type', '==|==|==', 'false|true|customslider' )
    ),
  )  
);

// -----------------------------------------
// Blog Template Section
// -----------------------------------------
$blog_template_section = array(
  'name'  => 'blog_template_section',
  'title' => esc_html__('Blog Template', 'aagan'),
  'icon'  => 'fa fa-files-o',
  'fields' =>  array(
    array(
      'id'           => 'blog-tpl-notice',
      'type'         => 'notice',
      'class'        => 'success',
      'content'      => esc_html__('Blog Tab Works only if page template set to Blog Template in Page Attributes','aagan'),
      'class'        => 'margin-30 cs-success',      
    ),
    array(
      'id'                     => 'blog-post-layout',
      'type'                   => 'image_select',
      'title'                  => esc_html__('Post Layout', 'aagan' ),
      'options'                => array(
          'one-column'         => AAGAN_THEME_URI . '/cs-framework-override/images/one-column.png',
          'one-half-column'    => AAGAN_THEME_URI . '/cs-framework-override/images/one-half-column.png',
          'one-third-column'   => AAGAN_THEME_URI . '/cs-framework-override/images/one-third-column.png',
		  '1-2-2'			   => AAGAN_THEME_URI . '/cs-framework-override/images/1-2-2.png',
		  '1-2-2-1-2-2' 	   => AAGAN_THEME_URI . '/cs-framework-override/images/1-2-2-1-2-2.png',
		  '1-3-3-3'			   => AAGAN_THEME_URI . '/cs-framework-override/images/1-3-3-3.png',
		  '1-3-3-3-1' 		   => AAGAN_THEME_URI . '/cs-framework-override/images/1-3-3-3-1.png',
      ),
      'default'                => 'one-half-column'
    ),
    array(
      'id'                     => 'blog-post-style',
      'type'                   => 'select',
      'title'                  => esc_html__('Post Style', 'aagan' ),
      'options'                => array(
        'blog-default-style' => esc_html__('Default','aagan'),
        'entry-date-left'    => esc_html__('Date Left','aagan'),
		'entry-date-left outer-frame-border'      	=> esc_html__('Date Left Modern', 'aagan'),
        'entry-date-author-left' => esc_html__('Date and Author Left','aagan'),
		'blog-modern-style'      => esc_html__('Modern', 'aagan'),
		'bordered'      		 => esc_html__('Bordered', 'aagan'),
		'classic'      			 => esc_html__('Classic', 'aagan'),
		'entry-overlay-style' 	 => esc_html__('Trendy', 'aagan'),
		'overlap' 				 => esc_html__('Overlap', 'aagan'),
		'entry-center-align'	 => esc_html__('Stripe', 'aagan'),
		'entry-fashion-style'	 => esc_html__('Fashion', 'aagan'),
		'entry-minimal-bordered' => esc_html__('Minimal Bordered', 'aagan'),
        'blog-medium-style'  	 => esc_html__('Medium','aagan'),
        'blog-medium-style dt-blog-medium-highlight' => esc_html__('Medium Highlight','aagan'),
        'blog-medium-style dt-blog-medium-highlight dt-sc-skin-highlight' => esc_html__('Medium Skin Highlight','aagan')
      ),
    ),
    array(
      'id'      => 'enable-blog-readmore',
      'type'    => 'switcher',
      'title'   => esc_html__('Read More', 'aagan' ),
      'default' => true
    ),
    array(
      'id'           => 'blog-readmore',
      'type'         => 'textarea',
      'title'        => esc_html__('Read More Shortcode', 'aagan' ),
      'default'      => '[dt_sc_button title="'.esc_attr__('Read More', 'aagan').'" style="filled" icon_type="fontawesome" iconalign="icon-right with-icon" iconclass="fa fa-long-arrow-right" class="type1" /]',
      'dependency'   => array( 'enable-blog-readmore', '==', 'true' ),
    ),
    array(
      'id'      => 'blog-post-excerpt',
      'type'    => 'switcher',
      'title'   => esc_html__('Allow Excerpt', 'aagan' ),
      'default' => true
    ),
    array(
      'id'           => 'blog-post-excerpt-length',
      'type'         => 'number',
      'title'        => esc_html__('Excerpt Length', 'aagan' ),
      'default'      => '45',
      'dependency'   => array( 'blog-post-excerpt', '==', 'true' ),
    ),
    array(
      'id'           => 'blog-post-per-page',
      'type'         => 'number',
      'title'        => esc_html__('Post Per Page', 'aagan' ),
      'default'      => '-1',      
    ),
    array(
      'id'             => 'blog-post-cats',
      'type'           => 'select',
      'title'          => esc_html__('Categories','aagan'),
      'options'        => 'categories',
      'default_option' => esc_html__('Select a categories','aagan'),
      'class'              => 'chosen',
      'attributes'         => array(
        'multiple'         => 'only-key',
        'style'            => 'width: 200px;'
      ),
      'info'           => esc_html__('Select categories to exclude from your blog page.','aagan'),
    ),
    array(
      'id'      => 'show-postformat-info',
      'type'    => 'switcher',
      'title'   => esc_html__('Show Post Format Info', 'aagan' ),
      'default' => true
    ),
    array(
      'id'      => 'show-author-info',
      'type'    => 'switcher',
      'title'   => esc_html__('Show Post Author Info', 'aagan' ),
      'default' => true,
    ),
    array(
      'id'      => 'show-date-info',
      'type'    => 'switcher',
      'title'   => esc_html__('Show Post Date Info', 'aagan' ),
      'default' => true
    ),
    array(
      'id'      => 'show-comment-info',
      'type'    => 'switcher',
      'title'   => esc_html__('Show Post Comment Info', 'aagan' ),
      'default' => true
    ),
    array(
      'id'      => 'show-category-info',
      'type'    => 'switcher',
      'title'   => esc_html__('Show Post Category Info', 'aagan' ),
      'default' => true
    ),
    array(
      'id'      => 'show-tag-info',
      'type'    => 'switcher',
      'title'   => esc_html__('Show Post Tag Info', 'aagan' ),
      'default' => true
    )    
  )
);

// -----------------------------------------
// Portfolio Template Section
// -----------------------------------------
$portfolio_template_section = array(
  'name'  => 'portfolio_template_section',
  'title' => esc_html__('Portfolio Template', 'aagan'),
  'icon'  => 'fa fa-picture-o',
  'fields' =>  array(

    array(
      'id'      => 'portfolio-tpl-notice',
      'type'    => 'notice',
      'class'   => 'success',
      'content' => esc_html__('Portfolio Tab Works only if page template set to Portfolio Template in Page Attributes','aagan'),
      'class'   => 'margin-30 cs-success',
    ),

    array(
      'id'                     => 'portfolio-post-layout',
      'type'                   => 'image_select',
      'title'                  => esc_html__('Post Layout', 'aagan' ),
      'options'                => array(
          'one-half-column'    => AAGAN_THEME_URI . '/cs-framework-override/images/one-half-column.png',
          'one-third-column'   => AAGAN_THEME_URI . '/cs-framework-override/images/one-third-column.png',
          'one-fourth-column'  => AAGAN_THEME_URI . '/cs-framework-override/images/one-fourth-column.png',
      ),
      'default'                => 'one-half-column'
    ),

    array(
      'id'      => 'portfolio-post-style',
      'type'    => 'select',
      'title'   => esc_html__('Post Style', 'aagan' ),
      'options' => array(
        'type1' => esc_html__('Modern Title','aagan'),
        'type2' => esc_html__('Title & Icons Overlay','aagan'),
        'type3' => esc_html__('Title Overlay','aagan'),
        'type4' => esc_html__('Icons Only','aagan'),
        'type5' => esc_html__('Classic','aagan'),
        'type6' => esc_html__('Minimal Icons','aagan'),
        'type7' => esc_html__('Presentation','aagan'),
        'type8' => esc_html__('Girly','aagan'),
        'type9' => esc_html__('Art','aagan'),
      ),
    ),

    array(
      'id'      => 'portfolio-grid-space',
      'type'    => 'switcher',
      'title'   => esc_html__('Allow Grid Space', 'aagan' ),
      'default' => true,
      'info'    => esc_html__('YES! to allow grid space in between portfolio item','aagan')
    ),

    array(
      'id'      => 'filter',
      'type'    => 'switcher',
      'title'   => esc_html__('Allow Filters', 'aagan' ),
      'default' => true,
      'info'    => esc_html__('YES! to allow filter options for portfolio items','aagan')
    ),

    array(
      'id'           => 'portfolio-post-per-page',
      'type'         => 'number',
      'title'        => esc_html__('Post Per Page', 'aagan' ),
      'default'      => '-1',      
    ),

    array(
      'id'             => 'portfolio-categories',
      'type'           => 'select',
      'title'          => esc_html__('Categories','aagan'),
      'options'        => 'categories',
      'class'          => 'chosen',
      'query_args'     => array(
        'type'         => 'dt_portfolios',
        'taxonomy'     => 'portfolio_entries',
        'orderby'      => 'post_date',
        'order'        => 'DESC',
      ),
      'attributes'         => array(
        'data-placeholder' => esc_html__('Select a categories','aagan'),
        'multiple'         => 'only-key',
        'style'            => 'width: 200px;'
      ),
      'info'           => esc_html__('Select categories to show in portfolio items.','aagan'),
    ),   
  )
);

// ===============================================================================================
// -----------------------------------------------------------------------------------------------
// METABOX OPTIONS
// -----------------------------------------------------------------------------------------------
// ===============================================================================================
$options = array();

// -----------------------------------------
// Page Metabox Options                    -
// -----------------------------------------
array_push( $meta_layout_section['fields'], array(
  'id'        => 'enable-sticky-sidebar',
  'type'      => 'switcher',
  'title'     => esc_html__('Enable Sticky Sidebar', 'aagan' ),
  'dependency'  => array( 'page-layout', 'any', 'with-left-sidebar,with-right-sidebar,with-both-sidebar' )
) );

$options[] = array(
	'id'        => '_tpl_default_settings',
    'title'     => esc_html__('Page Settings','aagan'),
    'post_type' => 'page',
    'context'   => 'normal',
    'priority'  => 'high',
    'sections'  => array(
		$meta_layout_section,
		$meta_breadcrumb_section,
		$meta_slider_section,

		$blog_template_section,
		$portfolio_template_section,
		array(
		  'name'  => 'sidenav_template_section',
		  'title' => esc_html__('Side Navigation Template', 'aagan'),
		  'icon'  => 'fa fa-th-list',
		  'fields' =>  array(

			array(
			  'id'           => 'sidenav-tpl-notice',
			  'type'         => 'notice',
			  'class'        => 'success',
			  'content'      => esc_html__('Side Navigation Tab Works only if page template set to Side Navigation Template in Page Attributes','aagan'),
			  'class'        => 'margin-30 cs-success',      
			),

			array(
			  'id'     		 => 'sidenav-style',
			  'type'    	 => 'select',
			  'title'   	 => esc_html__('Side Navigation Style', 'aagan' ),
			  'options'    => array(
				   'type1' => esc_html__('Type1','aagan'),
				   'type2' => esc_html__('Type2','aagan'),
				   'type3' => esc_html__('Type3','aagan'),
				   'type4' => esc_html__('Type4','aagan'),
				   'type5' => esc_html__('Type5','aagan'),
				   'type6' => esc_html__('Type6','aagan'),
				   'type7' => esc_html__('Type7','aagan'),
				   'type8' => esc_html__('Type8','aagan'),
				   'type9' => esc_html__('Type9','aagan'),
				   'type10' => esc_html__('Type10','aagan')
			  ),
			),

			array(
			  'id'    		 => 'sidenav-align',
			  'type'    	 => 'switcher',
			  'title'   	 => esc_html__('Align Right', 'aagan' ),
			  'info'    	 => esc_html__('YES! to align right of side navigation.','aagan')
			),

			array(
			  'id'    		 => 'sidenav-sticky',
			  'type'    	 => 'switcher',
			  'title'   	 => esc_html__('Sticky Side Navigation', 'aagan' ),
			  'info'    	 => esc_html__('YES! to sticky side navigation content.','aagan')
			),

			array(
			  'id'    		 => 'enable-sidenav-content',
			  'type'    	 => 'switcher',
			  'title'   	 => esc_html__('Show Content', 'aagan' ),
			  'info'    	 => esc_html__('YES! to show content in below side navigation.','aagan')
			),

			array(
			  'id'	    	 => 'sidenav-content',
			  'type'	     => 'textarea',
			  'title'  		 => esc_html__('Side Navigation Content', 'aagan' ),
			  'info'    	 => esc_html__('Paste any shortcode content here','aagan'),
			  'attributes' 	 => array(
				  'rows'     => 6,
			  ),
			),

		  )
		),
    )
);

// -----------------------------------------
// Post Metabox Options                    -
// -----------------------------------------
$post_meta_layout_section = $meta_layout_section;
$fields = $post_meta_layout_section['fields'];

	$fields[0]['title'] =  esc_html__('Post Layout', 'aagan' );
	unset( $fields[0]['options']['with-both-sidebar'] );
	unset( $fields[0]['info'] );
	unset( $fields[0]['options']['fullwidth'] );
	unset( $fields[5] );
	unset( $post_meta_layout_section['fields'] );
	$post_meta_layout_section['fields']  = $fields;  

	$post_format_section = array(
		'name'  => 'post_format_data_section',
		'title' => esc_html__('Post Format', 'aagan'),
		'icon'  => 'fa fa-cog',
		'fields' =>  array(

			array(
				'id'      => 'show-featured-image',
				'type'    => 'switcher',
				'title'   => esc_html__('Show Featured Image', 'aagan' ),
				'default' => true,
				'info'    => esc_html__('YES! to show featured image','aagan')
			),

			array(
				'id'           => 'single-post-style',
				'type'         => 'select',
				'title'        => esc_html__('Post Style', 'aagan'),
				'options'      => array(
				  'standard'      		=> esc_html__('Standard', 'aagan'),
				  'info-within-image'   => esc_html__('Info WithIn Image', 'aagan'),
				  'info-bottom-image'   => esc_html__('Info Over Image Bottom Left', 'aagan'),
				  'info-vertical-image' => esc_html__('Info Over Image Vertically Center', 'aagan'),
				  'info-above-image'    => esc_html__('Info Above Image', 'aagan'),
				  'single-flat' 		=> esc_html__('Flat', 'aagan')
				),
				'class'        => 'chosen',
				'default'      => 'standard',
				'info'         => esc_html__('Choose post style to display single post.', 'aagan')
			),

			array(
			    'id'           => 'view_count',
			    'type'         => 'text',
			    'title'        => esc_html__('Views', 'aagan' ),
				'info'         => esc_html__('No.of views of this post.', 'aagan')
			),

			array(
			    'id'           => 'like_count',
			    'type'         => 'text',
			    'title'        => esc_html__('Likes', 'aagan' ),
				'info'         => esc_html__('No.of likes of this post.', 'aagan')
			),

			array(
				'id' => 'post-format-type',
				'title'   => esc_html__('Type', 'aagan' ),
				'type' => 'select',
				'default' => 'standard',
				'options' => array(
					'standard'  => esc_html__('Standard', 'aagan'),
					'status'	=> esc_html__('Status','aagan'),
					'quote'		=> esc_html__('Quote','aagan'),
					'gallery'	=> esc_html__('Gallery','aagan'),
					'image'		=> esc_html__('Image','aagan'),
					'video'		=> esc_html__('Video','aagan'),
					'audio'		=> esc_html__('Audio','aagan'),
					'link'		=> esc_html__('Link','aagan'),
					'aside'		=> esc_html__('Aside','aagan'),
					'chat'		=> esc_html__('Chat','aagan')
				)
			),

			array(
				'id' 	  => 'post-gallery-items',
				'type'	  => 'gallery',
				'title'   => esc_html__('Add Images', 'aagan' ),
				'add_title'   => esc_html__('Add Images', 'aagan' ),
				'edit_title'  => esc_html__('Edit Images', 'aagan' ),
				'clear_title' => esc_html__('Remove Images', 'aagan' ),
				'dependency' => array( 'post-format-type', '==', 'gallery' ),
			),

			array(
				'id' 	  => 'media-type',
				'type'	  => 'select',
				'title'   => esc_html__('Select Type', 'aagan' ),
				'dependency' => array( 'post-format-type', 'any', 'video,audio' ),
		      	'options'	=> array(
					'oembed' => esc_html__('Oembed','aagan'),
					'self' => esc_html__('Self Hosted','aagan'),
				)
			),

			array(
				'id' 	  => 'media-url',
				'type'	  => 'textarea',
				'title'   => esc_html__('Media URL', 'aagan' ),
				'dependency' => array( 'post-format-type', 'any', 'video,audio' ),
			),
		)
	);

	$options[] = array(
		'id'        => '_dt_post_settings',
		'title'     => esc_html__('Post Settings','aagan'),
		'post_type' => 'post',
		'context'   => 'normal',
		'priority'  => 'high',
		'sections'  => array(
			$post_meta_layout_section,
			$meta_breadcrumb_section,
			$post_format_section			
		)
	);

// -----------------------------------------
// Tribe Events Post Metabox Options
// -----------------------------------------
  array_push( $post_meta_layout_section['fields'], array(
    'id' => 'event-post-style',
    'title'   => esc_html__('Post Style', 'aagan' ),
    'type' => 'select',
    'default' => 'type1',
    'options' => array(
      'type1'  => esc_html__('Classic', 'aagan'),
      'type2'  => esc_html__('Full Width','aagan'),
      'type3'  => esc_html__('Minimal Tab','aagan'),
      'type4'  => esc_html__('Clean','aagan'),
      'type5'  => esc_html__('Modern','aagan'),
    ),
	'class'    => 'chosen',
	'info'     => esc_html__('Your event post page show at most selected style.', 'aagan')
  ) );

  $options[] = array(
    'id'        => '_custom_settings',
    'title'     => esc_html__('Settings','aagan'),
    'post_type' => 'tribe_events',
    'context'   => 'normal',
    'priority'  => 'high',
    'sections'  => array(
      $post_meta_layout_section,
      $meta_breadcrumb_section
    )
  );


// -----------------------------------------
// Header And Footer Options Metabox
// -----------------------------------------
$post_types = apply_filters( 'aagan_header_footer_default_cpt' , array ( 'post', 'page' )  );
$options[] = array(
	'id'	=> '_dt_custom_settings',
	'title'	=> esc_html__('Header & Footer','aagan'),
	'post_type' => $post_types,
	'priority'  => 'high',
	'context'   => 'side', 
	'sections'  => array(
	
		# Header Settings
		array(
			'name'  => 'header_section',
			'title' => esc_html__('Header', 'aagan'),
			'icon'  => 'fa fa-angle-double-right',
			'fields' =>  array(
			
				# Header Show / Hide
				array(
					'id'		=> 'show-header',
					'type'		=> 'switcher',
					'title'		=> esc_html__('Show Header', 'aagan'),
					'default'	=>  true,
				),
				
				# Header
				array(
					'id'  		 => 'header',
					'type'  	 => 'select',
					'title' 	 => esc_html__('Choose Header', 'aagan'),
					'class'		 => 'chosen',
					'options'	 => 'posts',
					'query_args' => array(
						'post_type'	 => 'dt_headers',
						'orderby'	 => 'ID',
						'order'		 => 'ASC',
						'posts_per_page' => -1,
					),
					'default_option' => esc_attr__('Select Header', 'aagan'),
					'attributes' => array( 'style'	=> 'width:50%' ),
					'info'		 => esc_html__('Select custom header for this page.','aagan'),
					'dependency'	=> array( 'show-header', '==', 'true' )
				),							
			)			
		),
		# Header Settings

		# Footer Settings
		array(
			'name'  => 'footer_settings',
			'title' => esc_html__('Footer', 'aagan'),
			'icon'  => 'fa fa-angle-double-right',
			'fields' =>  array(
			
				# Footer Show / Hide
				array(
					'id'		=> 'show-footer',
					'type'		=> 'switcher',
					'title'		=> esc_html__('Show Footer', 'aagan'),
					'default'	=>  true,
				),
				
				# Footer
		        array(
					'id'         => 'footer',
					'type'       => 'select',
					'title'      => esc_html__('Choose Footer', 'aagan'),
					'class'      => 'chosen',
					'options'    => 'posts',
					'query_args' => array(
						'post_type'  => 'dt_footers',
						'orderby'    => 'ID',
						'order'      => 'ASC',
						'posts_per_page' => -1,
					),
					'default_option' => esc_attr__('Select Footer', 'aagan'),
					'attributes' => array( 'style'  => 'width:50%' ),
					'info'       => esc_html__('Select custom footer for this page.','aagan'),
					'dependency'    => array( 'show-footer', '==', 'true' )
				),			
			)			
		),
		# Footer Settings
		
	)	
);



	
CSFramework_Metabox::instance( $options );